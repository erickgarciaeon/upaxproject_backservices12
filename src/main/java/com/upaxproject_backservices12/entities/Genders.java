package com.upaxproject_backservices12.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Genders {

	@Id
	@GeneratedValue
	@Column(nullable=false)
	private int id;
	private String name;
	
	@OneToMany(targetEntity=Employees.class , mappedBy="gender_id_fk", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
