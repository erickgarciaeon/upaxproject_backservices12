package com.upaxproject_backservices12.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class EmployeeWorkedHours {

	@Id
	@GeneratedValue
	@Column(nullable=false)
	private int id;
	private int employee_id;
	private int worked_hours;
	private Date worker_date;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="employee_id_fk")
	
	public int getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}
	public int getWorked_hours() {
		return worked_hours;
	}
	public void setWorked_hours(int worked_hours) {
		this.worked_hours = worked_hours;
	}
	public Date getWorker_Date() {
		return worker_date;
	}
	public void setWorker_Date(Date worker_date) {
		this.worker_date = worker_date;
	}
	
	
	
}