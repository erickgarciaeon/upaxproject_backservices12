package com.upaxproject_backservices12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpaxprojectBackservices12Application {

	public static void main(String[] args) {
		SpringApplication.run(UpaxprojectBackservices12Application.class, args);
	}

}
