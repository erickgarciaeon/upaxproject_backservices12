package com.upaxproject_backservices12.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.upaxproject_backservices12.entities.EmployeeWorkedHours;
import com.upaxproject_backservices12.entities.Employees;

@Repository
public interface EmployeesRepository extends CrudRepository<Employees, Integer> {

	void save(EmployeeWorkedHours request);

}
