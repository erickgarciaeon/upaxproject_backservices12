package com.upaxproject_backservices12.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.upaxproject_backservices12.entities.EmployeeWorkedHours;

@Repository
public interface EmployeeWorkedHoursRepository extends CrudRepository<EmployeeWorkedHours, Integer> {

}
