package com.upaxproject_backservices12.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.upaxproject_backservices12.entities.EmployeeWorkedHours;

@Service
public interface IEmployeeWorkedHoursService extends CrudRepository<EmployeeWorkedHours, Integer>{

	
}
