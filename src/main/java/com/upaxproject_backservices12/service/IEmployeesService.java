package com.upaxproject_backservices12.service;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.upaxproject_backservices12.entities.Employees;

@Service
public interface IEmployeesService{
	
	List<Employees> list();

	void save(Employees request);

	Object findAll();
}
