package com.upaxproject_backservices12.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.upaxproject_backservices12.entities.EmployeeWorkedHours;
import com.upaxproject_backservices12.repository.EmployeesRepository;

@CrossOrigin("*")
@RestController
@RequestMapping("/EmployeeWorkedHours")
public class EmployeeWorkedHoursController {
	@Autowired
	private EmployeesRepository employeesRepository;

	@GetMapping(value = "/allhours", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> allhours(){
		return ResponseEntity.ok(this.employeesRepository.findAll());
	}
	
	@PostMapping(value = "/savehours", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> savehours(@RequestBody EmployeeWorkedHours request){
		this.employeesRepository.save(request);
		return ResponseEntity.ok(Boolean.TRUE) ;	
	}
}
