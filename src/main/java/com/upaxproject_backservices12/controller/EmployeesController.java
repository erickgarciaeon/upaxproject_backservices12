package com.upaxproject_backservices12.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.upaxproject_backservices12.entities.Employees;
import com.upaxproject_backservices12.repository.EmployeesRepository;
import com.upaxproject_backservices12.service.IEmployeesService;

@CrossOrigin("*")
@RestController
@RequestMapping("/employee")
public class EmployeesController {
	
	@Autowired
	private EmployeesRepository employeeRepository;

	@GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> all(){
		return ResponseEntity.ok(this.employeeRepository.findAll());
	}
	
	@PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> saveEmployee(@RequestBody Employees request){
		this.employeeRepository.save(request);
		return ResponseEntity.ok(Boolean.TRUE) ;	
	}
	

	
}
